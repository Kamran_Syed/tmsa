<?php 
/**
 * Theme Footer Section for our theme.
 * 
 * Displays all of the footer section and closing of the #main div.
 *
 * @package ThemeGrill
 * @subpackage Spacious
 * @since Spacious 1.0
 */
?>

		</div><!-- .inner-wrap -->
	</div><!-- #main -->	
	<?php do_action( 'spacious_before_footer' ); ?>
		<div class="tw-bs container agile_footer_pic">
			<div class="row">
				<div class="col-md-12">
					<h2 class="aspk_footer_logo">Our Students Have Been Featured In:</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
				<ul >
					<li class="aspk_foot"><img src="<?php echo get_stylesheet_directory_uri();?>/images/logo1.png"></li>
					<li class="aspk_foot"><img src="<?php echo get_stylesheet_directory_uri();?>/images/logo2.png"></li>
					<li class="aspk_foot"><img src="<?php echo get_stylesheet_directory_uri();?>/images/logo3.png"></li>
					<li class="aspk_foot"><img src="<?php echo get_stylesheet_directory_uri();?>/images/logo4.png"></li>
					<li class="aspk_foot"><img src="<?php echo get_stylesheet_directory_uri();?>/images/logo5.png"></li>
					<li class="aspk_foot"><img src="<?php echo get_stylesheet_directory_uri();?>/images/logo6.png"></li>
					<li class="aspk_foot"><img src="<?php echo get_stylesheet_directory_uri();?>/images/logo7.png"></li>
					<li class="aspk_foot"><img src="<?php echo get_stylesheet_directory_uri();?>/images/forbes-logo.png"></li>
				</ul>
				</div>
			</div>
		</div>
		<footer id="colophon" class="clearfix">	
			<?php get_sidebar( 'footer' ); ?>	
			<div class="footer-socket-wrapper clearfix agile_footer_bgcolor" >
			<!--	<div class="inner-wrap">
						<div class="footer-socket-area">
							<?php do_action( 'spacious_footer_copyright' ); ?>
							<nav class="small-menu clearfix">
								<?php
									if ( has_nav_menu( 'footer' ) ) {									
											wp_nav_menu( array( 'theme_location' => 'footer',
																	 'depth'           => -1
																	 ) );
									}
								?>
							</nav>
						</div>
					</div>-->
					<div class="tw-bs container">
						<div class="row">
							<div class="col-md-3"><h3 class="widget-title">Categories</h3>
									<?php
										if ( has_nav_menu( 'footer' ) ) {									
											wp_nav_menu( array( 'theme_location' => 'footer',
																	 'class'           => 'aspk_foot_nav'
																	 ) );
										}
									?>
							</div>
							<div class="col-md-5 aspk_footer_col"><h3 class="widget-title">reviews</h3>
								<div >
									<p class="testi_msg">Finding Eben was a moment that literally changed my life. Personally, and professionally, I can honestly say I would not be where I am today without him.</p>
								</div>
							</div>
							<div class="col-md-4"><h3 class="widget-title">contact us</h3>
								<div class="textwidget"><p style="text-align: left; float: left;font-size:14px;">3960 Howard Hughes Pkwy, 5th Floor Las Vegas, NV 89109</p><br/>
									<p style="text-align: left; font-size:14px;">Email: <a href="mailto:support@getaltitude.com" onclick="ga('send', 'event', 'mailto', 'support@getaltitude.com');">support@getaltitude.com</a></p><br/>
								</div>
								<div><p style="text-align: left; font-size:14px;">Phone:  888-862-8349</p></div>
								<div style="margin-top:1em;">
									<span><a class="orange-btn" href="#"><input type="button" style="width: 13em;height: 3em;background-color: #F84707;color:white;" value="contact us"></a></span>
								</div>
							</div>
						</div>
					</div>
			</div>			
		</footer>
		<a href="#masthead" id="scroll-up"></a>	
	</div><!-- #page -->
	<?php wp_footer(); ?>
</body>
</html>