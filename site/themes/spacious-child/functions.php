<?php
add_action( 'after_setup_theme', 'spacious_setup_child' );
add_action( 'widgets_init', 'flatsome_widgets_init' );
function flatsome_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar Top Search', 'flatsome' ),
		'id'            => 'sidebar-main-top',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3><div class="tx-div small"></div>',
	) );
}
function spacious_setup_child() {

	// Registering navigation menus.
	register_nav_menus( array(
		'top_header' 		=> 'Top Header',
		'top_footer' 		=> 'Top Footer',
		'sub_header_menu' 	=> 'Sub header menu'
	) );
}
add_action( 'wp_enqueue_scripts', 'wpa_custom_css', 999 );
function wpa_custom_css(){
    wp_enqueue_style(
        'wpa_custom_bootstrap',
        get_stylesheet_directory_uri() . '/css/agile-bootstrap.css'
    );
    wp_enqueue_style('jquery_ui_min_css', get_stylesheet_directory_uri().'/css/jquery-ui.min.css');
    wp_enqueue_script('jquery_ui_min_js', get_stylesheet_directory_uri().'/js/jquery-ui.min.js');
}
add_action( 'widgets_init','agile_tmsa_widget');

function agile_tmsa_widget(){ 
	register_widget( 'agile_tmsa_widget' );
}

class Agile_Tmsa_Widget extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'agile_tmsa_widget', // Base ID
			__('Tmsa subscription widget', 'text_domain'), // Name
			array( 'description' => __( 'Displays subscription widget in sidebar ', 'text_domain' ), ) // Args
		);
		
	}

	public function widget( $args, $instance ) { 
		?>
		  <script>
		    jQuery(function() {
				jQuery("#tabs").tabs({
				  show: { effect: "slide", duration: 1000 }
				});
			});
		  </script>
		<div id="tabs">
		  <ul>
			<li><a style="padding:0.3em" href="#tabs-1">Magazine</a></li>
			<li><a style="padding:0.3em" href="#tabs-2">iPad</a></li>
			<li><a style="padding:0.3em" href="#tabs-3">Desktop</a></li>
		  </ul>
		  <div style="height:19em;" id="tabs-1">
			<img src="<?php echo get_stylesheet_directory_uri().'/images/current-issue.png'; ?>"/>
			<p style="font-size:14px;">March 2015: The Great Washington Bucket List</p>
			<div style="float:left;clear:left;">
				<div style="float:left;clear:left;">
					<img style="width:150px;" src="<?php echo get_stylesheet_directory_uri().'/images/toc-2015_March.jpg'; ?>"/>
				</div>
				<div style="float:left;margin-left:1em;">
					<div style="text-align:center;padding:0.1em;border-radius: 0.2em;background-color: black;">
						<a style="color:white;" href="https://www.washingtonian.com/magazine/subscribe/?src=issuebox_btn" target="_parent" id="subscribe">Subscribe</a>
					</div>
                    <ul>
                        <li style="list-style: none; padding: 6px 0 6px 0; border-top: 1px solid #d8d8d8; border-bottom: 1px solid #d8d8d8;margin-top: -1px;"><a href="/articles/cover-archive/march-2015-contents-the-great-washington-bucket-list/index.php" target="_parent">Table of Contents</a></li>
                        <li style="list-style: none; padding: 6px 0 6px 0; border-top: 1px solid #d8d8d8; border-bottom: 1px solid #d8d8d8;margin-top: -1px;"><a href="https://w1.buysub.com/servlet/GiftsGateway?cds_mag_code=WSH&amp;cds_page_id=105791" target="_parent">Give a Gift</a></li>
						<li style="list-style: none; padding: 6px 0 6px 0; border-top: 1px solid #d8d8d8; border-bottom: 1px solid #d8d8d8;margin-top: -1px;"><a href="/2015-archive/index.php" target="_parent">Cover Archive</a></li>
                        <li style="list-style: none; padding: 6px 0 6px 0; border-top: 1px solid #d8d8d8; border-bottom: 1px solid #d8d8d8;margin-top: -1px;"><a href="https://w1.buysub.com/servlet/CSGateway?cds_mag_code=WSH&amp;cds_page_id=22265" target="_parent">Subscriber Services</a></li>
					</ul>
                </div>
			</div>
		  </div>
		  <div style="height:19em;" id="tabs-2">
			<img src="<?php echo get_stylesheet_directory_uri().'/images/ipad-edition.png'; ?>"/>
			<div style="float:left;clear:left;">
				<div style="float:left;clear:left;">
					<img style="width:150px;" src="<?php echo get_stylesheet_directory_uri().'/images/March2015iPad-cover.png'; ?>"/>
				</div>
				<div style="float:left;width:10em;margin-left:1em;">
					<div style="float:left;"><p>Read the magazine Washington lives by on your iPad.It's free if you are a print subscriber!</p>
					</div>
					<div style="padding:0.1em;border-radius: 0.2em;float:left;clear:left;background-color:black;color:white;"><a style="color:white;" href="">Download</a></div>
				</div>
			</div>
			<p style="clear:both;"><small><a href="" target="_parent">Or subscribe now </a> for <strong>print + iPad + digital access</strong></small></p>
		  </div>
		  <div style="height:19em;" id="tabs-3">
			<a href="" target="_blank">
				<img src="<?php echo get_stylesheet_directory_uri().'/images/desktop-edition.png'; ?>"/>
			</a>
			<a id="subscribe" style="width: 30%; margin: -29px auto; padding-top:6px; position:relative; z-index:5; top: 126px; box-shadow:0 0 7px #777;display: block;    background-color: #000;    color: #FAFAFA;    font-size: 16.5px;    line-height: 18px;    padding: 4px 3px;    text-align: center;    margin-bottom: 10px;    border-radius: 5px;    font-weight: normal;    box-shadow: 0px 1px 1px #ccc;left: -52px;" href="" target="_blank">READ NOW</a>
			
			<div style="float:left;clear:left;">
				<img width="250" style="padding-top:9px; display:block;margin:0 auto;" src="<?php echo get_stylesheet_directory_uri().'/images/March2015desktop-promo.png'; ?>"/>
			</div>
			<div style="float:left;clear:left;">
				<p style="font-size:13px; line-height:14.5px; text-align:center;">Read the magazine Washington lives by on your computer. It's free for print subscribers.</p>
			</div>
			<div style="float:left;clear:left;">
				<p style="text-align:center;"><small><a href="" target="_parent">Or subscribe now </a> for <strong>print + iPad + digital access</strong></small></p>
			</div>
		  </div>
		</div>
		<?php

	}
	public function form( $instance ) {
		
		
	}

	public function update( $new_instance, $old_instance ) {
		
	}
}