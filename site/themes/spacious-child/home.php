<?php
/*
Template Name: Home Page
*/
?>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri().'/css/bootstrap.css';  ?>">
<?php
/**
 * This hook is important for wordpress plugins and other many things
 */
wp_head();
?>
</head>
<body <?php body_class(); ?>>
<?php	do_action( 'before' ); ?>
<div id="page" class="hfeed site">
	<?php do_action( 'spacious_before_header' ); ?>
	<header id="masthead" class="site-header clearfix agile_header_image">
			<div id="header-text-nav-container" >
				<div class="inner-wrap">
					<div class="inner-wrap11 row">
						<div class="col-md-8">	
							<div id="header-left-section">
								<?php
								if( ( of_get_option( 'spacious_show_header_logo_text', 'text_only' ) == 'both' || of_get_option( 'spacious_show_header_logo_text', 'text_only' ) == 'logo_only' ) && of_get_option( 'spacious_header_logo_image', '' ) != '' ) {
								?>
									<div id="header-logo-image">
										<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo of_get_option( 'spacious_header_logo_image', '' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"></a>
									</div><!-- #header-logo-image -->
								<?php
								}
								?>
							</div><!-- #header-left-section -->
						</div>
						<div class="col-md-4">	
							<div id="header-right-section" class="header_right_login">
							<?php
								$page4 = get_page_by_title( 'Login' );
								$login_link = get_permalink( $page4->ID ); 
							?>
							 <a href="<?php echo $login_link; ?>" class="aspk_login">LOGIN</a>
							</div>
						</div>
					</div>
					<div class="inner-wrap11 row">
						<div class="col-md-10" style="margin-bottom:1em;">	
							<div id="header-right-section">
								<nav id="site-navigation" class="main-navigation" role="navigation">
									<h3 class="menu-toggle"><?php _e( 'Menu', 'spacious' ); ?></h3>
									<?php
										if ( has_nav_menu( 'primary' ) ) {
											wp_nav_menu( array( 'theme_location' => 'primary' ) );
										}
									?>
								</nav>
							</div><!-- #header-right-section -->
						</div>
						<div class="col-md-2" style="margin-bottom:1em;margin-top:1.3em;">
							<?php if ( is_active_sidebar( 'sidebar-main-top' ) ){
									dynamic_sidebar( 'sidebar-main-top' );
								}
							?>
						</div>
				    </div>
				</div>
			</div><!-- .inner-wrap -->
			<div class="container">	
				<div class="row"> 
					<div class="col-md-12">
						  <h2 class="headline">Grow Your Business, Profit &amp; Income<br>
							Which do you want to <span class="blue-highlight">grow today</span>?</h2>
							<div style="margin: 5px 100px 130px;"><img  src="<?php echo get_stylesheet_directory_uri();?>/images/Broken-Line.png"></div>
					</div>
				</div>
			</div>
	</header>
	<div id="main" class="clearfix">
		<div class="inner-wrap">
			<div id="content" class="clearfix">

				<?php if ( have_posts() ) { ?>

					<?php while ( have_posts() ) : the_post(); ?>
					
					<?php the_content(); ?>
				<div class="container">	
				<?php
					$paget = get_page_by_title( 'GET THE TACTICS' );
					$tactics_link = get_permalink( $paget->ID ); 
					$pagetc = get_page_by_title( 'Get Started Today' );
					$started_link = get_permalink( $pagetc->ID ); 
					$pageonline = get_page_by_title( 'Start a Business Online' );
					$online_link = get_permalink( $pageonline->ID ); 
				?>
					<div class="row"> 
					  <!-- Modal 1 -->
						<div class="col-md-4">
							<div class="homepage-featured-box box-one">
							  <h3 class="h-heading">Start A Business Online</h3>
							  <div class="col-sm-12"> 
								
								<!--<h4>Start My Business</h4>--> 
								
								&nbsp;
								<p>Make money using knowledge and talent you already have to create products you can sell for high prices online</p>
								<a class="lbp-inline-link-1 download-btn cboxElement" href="<?php echo $online_link; ?>" >Start a Business</a>
							  </div>
							</div>
						</div>
					  <!-- Modal 1 --> 
					  <!-- Modal 2 -->
						<div class="col-md-4">
							<div class="homepage-featured-box box-two">
							  <h3 class="h-heading">Grow Your Business</h3>
							  <div class="col-sm-12"> 
								
								<!--<h4>Grow My Business</h4>--> 
								
								&nbsp;
								<p>Advanced systems and strategies to rapidly grow your business to the 6 and 7 figure per year level&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
								<a class="lbp-inline-link-2 download-btn cboxElement" href="<?php  echo $started_link; ?>" >get started today</a>
							  </div>
							</div>
						</div>
					  <!-- Modal 2 --> 
					  <!-- Modal 3-->
						<div class="col-md-4">
							<div class="homepage-featured-box box-three">
							  <h3 class="h-heading">Get More Customers</h3>
							  <div class="col-sm-12"> 
								
								<!--<h4>Start My Business</h4>--> 
								
								&nbsp;
								<p>Marketing tactics and fill in the blank templates to drive traffic to your business that will convert into paying customers</p>
								<a class="lbp-inline-link-3 download-btn cboxElement" href="<?php  echo $tactics_link; ?>" >get the tactics</a>
							  </div>
							</div>
						</div>
				   </div>
				   <div class="row"> 
						<div class="col-md-12">
							<div class="testimonial-slider-section col-sm-12"> 

								<div class="testi-msg">
									<p>Finding Eben was a moment that literally changed my life. Personally, and professionally, I can honestly say I would not be where I am today without him.</p>
								</div>				

							</div><!-- testi-author -->
						</div>
					</div>
					<div id="show_black_form_screen">
						
					
					</div>
					<script>
						/* jQuery( ".download-btn" ).click(function() {
							jQuery("#show_black_form_screen").show();
						});
						jQuery( "#show_black_form_screen" ).click(function() {
							jQuery("#show_black_form_screen").hide();
						}); */
							
					</script>

						<?php endwhile; ?>

						
						
					<?php } ?>
				</div><!-- end container -->

			</div><!-- #content -->

<?php get_footer(); ?>