<?php
/**
 * Theme Header Section for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main" class="clearfix"> <div class="inner-wrap">
 *
 * @package ThemeGrill
 * @subpackage Spacious
 * @since Spacious 1.0
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri().'/css/bootstrap.css';  ?>">
<?php
/**
 * This hook is important for wordpress plugins and other many things
 */
wp_head();
?>
</head>

<body <?php body_class(); ?>>
<?php	do_action( 'before' ); ?>
<div id="page" class="hfeed site">
	<?php do_action( 'spacious_before_header' ); ?>
	<header id="masthead" class="site-header clearfix">
		<?php if( of_get_option( 'spacious_header_image_position', 'above' ) == 'above' ) { spacious_render_header_image(); } ?>

		<div id="header-text-nav-container" style="background-color:black;">
			<div class="inner-wrap">
				<div class="inner-wrap11 row">
					<div class="col-md-8">	
						<div id="header-left-section">
							<?php
							if( ( of_get_option( 'spacious_show_header_logo_text', 'text_only' ) == 'both' || of_get_option( 'spacious_show_header_logo_text', 'text_only' ) == 'logo_only' ) && of_get_option( 'spacious_header_logo_image', '' ) != '' ) {
							?>
								<div id="header-logo-image">
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo of_get_option( 'spacious_header_logo_image', '' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"></a>
								</div><!-- #header-logo-image -->
							<?php
							}
							?>
						</div><!-- #header-left-section -->
					</div>
					<div class="col-md-4">	
						<div id="header-right-section" class="header_right_login">
						<?php
							$page4 = get_page_by_title( 'Login' );
							$login_link = get_permalink( $page4->ID ); 
						?>
						 <a href="<?php echo $login_link; ?>" class="aspk_login">LOGIN</a>
						</div>
					</div>
				</div>
				<div class="inner-wrap11 row">
					<div class="col-md-10" style="margin-bottom:1em;">	
						<div id="header-right-section">
							<nav id="site-navigation" class="main-navigation" role="navigation">
								<h3 class="menu-toggle"><?php _e( 'Menu', 'spacious' ); ?></h3>
								<?php
									if ( has_nav_menu( 'primary' ) ) {
										wp_nav_menu( array( 'theme_location' => 'primary' ) );
									}
								?>
							</nav>
						</div><!-- #header-right-section -->
					</div>
					<div class="col-md-2" style="margin-bottom:1em;margin-top:1.3em;">
						<?php if ( is_active_sidebar( 'sidebar-main-top' ) ){
								dynamic_sidebar( 'sidebar-main-top' );
							}
						?>
					</div>
				</div>
			</div><!-- .inner-wrap -->
			<nav id="sub-site-navigation" class="main-navigation aspk_sub_men" role="navigation">
				<h3 class="menu-toggle"><?php _e( 'Menu', 'spacious' ); ?></h3>
				<?php
					if ( has_nav_menu( 'sub_header_menu' ) ) {
						wp_nav_menu( array( 'theme_location' => 'sub_header_menu' ) );
					}
				?>
				
			</nav>
		</div>

		<?php if( of_get_option( 'spacious_header_image_position', 'above' ) == 'below' ) { spacious_render_header_image(); } ?>

		<?php
   	if( of_get_option( 'spacious_activate_slider', '0' ) == '1' ) {
   		if( of_get_option( 'spacious_blog_slider', '0' ) == '0' ) {
   			if( is_home() || is_front_page() ) {
   				spacious_featured_image_slider();
			}
   		} else {
   			if( is_front_page() ) {
   				spacious_featured_image_slider();
   			}
   		}
   	}
		?>
	</header>
	<?php do_action( 'spacious_after_header' ); ?>
	<?php do_action( 'spacious_before_main' ); ?>
	<div id="main" class="clearfix">
		<div class="inner-wrap">